import logging
import asyncio
import os
import time
from typing import Any, Dict, List

import httpx
#from supabase_py import Client, create_client
#from supabase_py.lib.query_builder import SupabaseQueryBuilder
from postgrest_py.request_builder import QueryRequestBuilder, RequestBuilder
from postgrest_py.client import PostgrestClient

import game
import utils

# Monkey patching 

def from_(self, table: str) -> RequestBuilder:
    """Perform a table operation."""
    self.session.params = httpx.QueryParams(None)
    return RequestBuilder(self.session, f"/{table}")

PostgrestClient.from_ = from_


url: str = os.environ.get("SUPABASE_URL")
key: str = os.environ.get("SUPABASE_KEY")
#supabase: Client = create_client(url, key)
supabase: PostgrestClient = PostgrestClient(f"{url}/rest/v1/")
supabase.auth(token=key)
supabase.session.headers["apikey"] = key

logger = logging.getLogger(__name__)


async def list_games():
    return (await supabase.from_("game").select("*").execute())

async def insert_game(db_id: int, game: game.LeetxGame):
    data = {
        "leetx_id": game.id,
        "name": game.name,
        "version": game.version,
        "type": game.type,
        "hash": game.hash,
        #"genres": ";".join(game.genres),
        #"languages": ";".join(game.languages),
        "description": game.description,
        "nsfw": game.nsfw,
    }

    if (
        len(
            (await supabase.from_("game")
            .select("*")
            .eq("leetx_id", str(game.id))
            .execute())
        )
        != 0
    ):
        data["id"] = (
            (await supabase.from_("game")
            .select("id")
            .eq("leetx_id", str(game.id))
            .execute())[0]["id"]
        )
        print(f"Updating {data['id']} -> {game.id}")
    else:
        data["id"] = db_id
        print(f"Adding new game {game.id}")


    try:
        #await asyncio.gather(
        await supabase.from_("game").insert(data, upsert=True).execute(),
        await insert_languages(str(data["id"]), utils.fix_languages(game.languages)),
        await insert_genres(str(data["id"]), utils.titleify(game.genres)),
            #insert_tags(str(id), utils.titleify(game.tags))
        #)
    except Exception as e:
        print(e)


async def delete_game(id: int):
    try:
        print(f"Removing game {id}")
        try:
            await supabase.from_("game_genre").delete().eq("game_id", str(id)).execute();
        except:
            pass

        try:
            await supabase.from_("game_language").delete().eq("game_id", str(id)).execute();
        except:
            pass
        await supabase.from_("game").delete().eq("id", str(id)).execute();
        print(f"Removed game {id}")
    except Exception as e:
        print(e)


async def get_game_id_from_1337x_id(leetx_id: str) -> str:
    return (
        (await supabase.from_("game")
        .select("id")
        .eq("1337x_id", leetx_id)
        .execute())[0]["id"]
    )

async def insert_in_junction(game_id: str, items: List[str], item_table: str, junction_table: str):
    for item in items:
        item = item.strip().title()
        if item is None or item == "":
            continue

        if (
            len(
                (await supabase.from_(item_table)
                .select("*")
                .eq("name", item)
                .execute())
            )
            == 0
        ):
            print(f"New {item_table}: {item}")
            await supabase.from_(item_table).insert({"name": item}).execute()

        item_id = (
            (await supabase.from_(item_table)
            .select("id")
            .eq("name", item)
            .execute())[0]["id"]
        )

        if (
            len(
                (await supabase.from_(junction_table)
                .select("*")
                .eq(f"{item_table}_id", str(item_id))
                .eq("game_id", str(game_id))
                .execute())
            )
            == 0
        ):
            print(f"Added {item_table} ({game_id}, {item_id})")
            print(
                await supabase.from_(junction_table)
                .insert({f"{item_table}_id": item_id, "game_id": game_id})
                .execute()
            )
        else:
            print(f"Found item ({game_id}, {item_id})")



async def insert_languages(game_id: str, languages: List[str]):
    await insert_in_junction(game_id, languages, "language", "game_language")

async def insert_genres(game_id: str, genres: List[str]):
    await insert_in_junction(game_id, genres, "genre", "game_genre")

async def insert_tags(game_id: str, tags: List[str]):
    await insert_in_junction(game_id, tags, "tag", "game_tag")
