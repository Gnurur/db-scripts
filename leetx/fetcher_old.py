import logging
import re
from typing import Generator

import httpx
from bs4 import BeautifulSoup

import game

logger = logging.getLogger(__name__)

BASE_URL = "https://1337x.to"
USER = "johncena141"
SEARCH_URL = f"{BASE_URL}/search/{{}}+gnu+linux/1/"
GAMES_URL = f"{BASE_URL}/{USER}-torrents/{{}}/"

GAMES_PER_PAGE = 100


class LeetxColumns:
    """Represents a column in Leetx search results."""

    TITLE: int = 0
    SEEDS: int = 1
    LEECHS: int = 2
    SIZE: int = 3


def search_games(client, query: str):
    return fetch_games(client, SEARCH_URL.format(query))


def get_games_on_page(client, page_no: int):
    return fetch_games(client, GAMES_URL.format(page_no))


async def fetch_games(client, url: str):

    # parse the html
    resp = await client.get(url)

    html = resp.text
    soup = BeautifulSoup(html, "lxml")

    # find the table with list of torrents
    table = soup.find("table")
    if not table:
        return

    table = table.tbody
    for row in table.find_all("tr"):
        cols = row.find_all("td")

        link = BASE_URL + cols[LeetxColumns.TITLE].find_all("a")[1].get("href").strip()
        title = cols[LeetxColumns.TITLE].get_text().strip()
        seeds = cols[LeetxColumns.SEEDS].get_text().strip()
        leechs = cols[LeetxColumns.LEECHS].get_text().strip()
        size = cols[LeetxColumns.SIZE].get_text()[:-1].strip()
        regex = re.compile(r".*\/(\d*)\/.*")
        match = regex.search(link)
        if match:
            game_id = match.group(1).strip()
        else:
            game_id = "Unknown"

        # append the dictionary to the games list
        yield game.LeetxGame(
            id=game_id,
            title=title,
            link=link,
            seeds=seeds,
            leechs=leechs,
            size=size,
        )
