import logging
from typing import Any, List

import database

logger = logging.getLogger(__name__)


def flatten(t: List[Any]) -> List:
    return [item for sublist in t for item in sublist]


def fix_languages(languages: List[str]) -> List[str]:
    real_langs = []
    for lang in languages:
        lang = lang.strip().title()
        lang = lang.strip("*")
        for l in lang.split(" "):
            l = l.strip("*.")

            if (
                l == "Chinese"
                or l == "And"
                or l == "-"
                or l == "Spain"
                or l == "Brazil"
                or l == ""
            ):
                continue
            elif l == "Traditional" or l == "Simplified":
                l += " Chinese"
            real_langs.append(l.strip())

    if len(real_langs) == 0:
        real_langs.append("English")

    return real_langs


def titleify(items: List[str]) -> List[str]:
    return [item.strip().title() for item in items]

