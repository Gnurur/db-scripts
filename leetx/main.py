import asyncio
import logging
import os
from pprint import pprint

import httpx

import database
import fetcher_old
import utils

logger = logging.getLevelName(__name__)
#logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"))

cols = [
    "id",
    "1337x_id",
    "name",
    "version",
    "type",
    "hash",
    "genres",
    "languages",
    "description",
    "nsfw",
    "sys_reqs",
]


# wrapper coroutine to speed up requests
async def get_page(client, page):
    print(page)
    games = set()
    async for game in fetcher_old.get_games_on_page(client, page):
        await game.setup(client)
        games.add(game)
    return games


async def remove_duplicates():
    db_games = await database.list_games()
    duplicates = []
    for game1 in db_games:
        for game2 in db_games:
            if (game1["id"] == game2["id"]):
                continue
            if (
                game1["leetx_id"] == game2["leetx_id"]
            ):
                duplicates.append(
                    game1 if game1["id"] > game2["id"] else game2
                )
            if (
                game1["name"] == game2["name"]
            ):
                duplicates.append(
                    game1 if game1["leetx_id"] < game2["leetx_id"] else game2
                )

    ids = set([game["id"] for game in duplicates])
    await asyncio.gather(*[database.delete_game(id) for id in ids])

async def update(force=False):
    # get db games
    db_games = tuple(await database.list_games())
    db_ids = set([game["leetx_id"] for game in db_games])
    # db new id starts at
    id_start = max([game["id"] for game in db_games]) + 1

    # get leetx_games
    leetx_games = set()
    tasks = []
    async with httpx.AsyncClient() as client:
        for page in range(1, 4):
            tasks.append(asyncio.create_task(get_page(client, page)))
        leetx_games = utils.flatten(await asyncio.gather(*tasks))
        leetx_ids = set([int(game.id) for game in leetx_games])

    # add games in leetx that are not in db
    #print(db_ids)
    if not force:
        new_ids = leetx_ids - db_ids
    else:
        new_ids = leetx_ids
    new_games = set([game for game in leetx_games if int(game.id) in new_ids])
    #await asyncio.gather(*[database.insert_game(game1) for id, game1 in enumerate(new_games)])
    for game in new_games:
        await database.insert_game(id_start, game)
        id_start += 1

async def gather_with_concurrency(n, *tasks):
    semaphore = asyncio.Semaphore(n)

    async def sem_task(task):
        async with semaphore:
            return await task
    return await asyncio.gather(*(sem_task(task) for task in tasks))

async def update_all_game_props():
    """Fixes languages and genres from existing games"""
    db_games = tuple(await database.list_games())

    await gather_with_concurrency(100,
            *[database.insert_languages(str(game["id"]), utils.fix_languages(game["languages"].split(";"))) for game in db_games], 
            *[database.insert_genres(str(game["id"]), utils.titleify(game["genres"].split(";"))) for game in db_games]
    )


async def main():
    await remove_duplicates()
    await update(force=True)
    #await update_all_game_props()


if __name__ == "__main__":
    asyncio.run(main())
