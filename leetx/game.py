import logging
import re
import string
from dataclasses import dataclass, field
from pathlib import Path

import httpx
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)

BANNER_LIST_URL = (
    "https://gitlab.com/chad-productions/chad_launcher_banners/-/raw/master/list.txt"
)


@dataclass
class LeetxGame:
    """Represents a game from leetx."""

    id: str
    title: str
    link: str
    seeds: str
    leechs: str
    size: str
    magnet: str = ""
    nsfw: bool = field(init=False, default=False)
    have_info: bool = field(init=False, default=False)
    genres: list = field(init=False, default_factory=list)
    languages: list = field(init=False, default_factory=list)
    description: str = ""
    sys_reqs: dict = field(init=False, default_factory=dict)
    type: str = field(init=False)

    def __hash__(self) -> int:
        return hash(self.id)

    def __post_init__(self):
        pass

    def __repr__(self) -> str:
        return self.id

    async def setup(self, client: httpx.AsyncClient, force: bool = False) -> None:
        """
        Method to setup game data like name, description, banner image etc...

        Parameters
        ----------
        session : ClientSession
            aiohttp.ClientSession object to use when making network requests.
        force : bool
            if the info should be fetched even if it's already present.
        """

        # get game info
        self.parse_info()
        if not self.have_info or force:
            await self.get_info(client)

        # download game banner if not already present or if forcing a refresh
        # if not self.banner._path.is_file() or force:
        #     await self.banner.fetch(self.name, client)

    def parse_info(self) -> None:
        """Parses game info from Leetx Title."""
        regex = re.compile(r"v?\d+[\.\w]*")
        self.name = self.title
        if "-" in self.name:
            self.name = self.name[: self.name.index("-")].strip()
        elif "[" in self.name:
            self.name = self.title[: self.title.index("[")].strip()
            # remove version from name
            self.name = regex.sub("", self.name)

        # get version
        match = regex.search(self.title)
        if "collection" in self.name.lower():
            self.version = "collection"
        elif "+" in self.name:
            self.version = "multi"
        else:
            if match is not None:
                self.version = match.group(0)
            else:
                self.version = "original"

        for char in self.name:
            if not char.isalnum():
                self.name.replace(char, "")

        # get nsfw status:
        if "[18+]" in self.title:
            self.nsfw = True
        if "[+18]" in self.title:
            self.nsfw = True
        # get type
        self.type = "native"
        if "wine" in self.title.lower():
            self.type = "wine"

    async def get_info(self, client: httpx.AsyncClient) -> None:
        """
        Gets and parses gameinfo from Leetx Torrent Page.

        Parameters
        ----------
        session : ClientSession
            aiohttp.ClientSession object to use when making network requests.
        """
        # get the html
        try:
            resp = await client.get(self.link)
        except Exception as e:
            print(e)
            logger.warn("could not connect to 1337x.to")
            return

        # parse the html
        html = resp.text
        soup = BeautifulSoup(html, "lxml")
        # replace <br\> tags with newlines.
        for br in soup("br"):
            br.replace_with("\n")

        # get game info.
        infos = soup.find("div", attrs={"id": "description"})
        if not infos:
            return

        infos = infos.get_text()
        infos = [x.strip() for x in infos.split("\n\n")][:-3]

        # find game description and sys reqs and genres
        _infos = sys_reqs_str = ""
        for i, info in enumerate(infos):
            if info.lower().startswith("description"):
                self.description = "\n".join(
                    "\n\n".join(infos[i:]).split("\n")[1:]
                ).strip()
            if info.lower().startswith("system requirements"):
                sys_reqs_str = "\n".join(infos[i].split("\n")[1:]).strip()
            if info.lower().startswith("info"):
                _infos = info.split("\n")

        # parse description
        printable = set(string.printable)
        self.description = "".join(filter(lambda x: x in printable, self.description))

        # convert sys reqs to dictionary
        self.sys_reqs = {}
        for line in sys_reqs_str.split("\n"):
            if ":" in line:
                key, value = line.split(":", 1)
            else:
                key = "Additional Info"
                value = line
            self.sys_reqs[key.strip()] = value.strip()

        # parse info
        for info in _infos:
            if info.lower().startswith("genre"):
                self.genres = [x.strip() for x in info.split(":")[1].split(",")]
            if info.lower().startswith("language"):
                self.languages = [x.strip() for x in info.split(":")[1].split(",")]

        self.magnet = soup.find("a", href=re.compile("magnet")).get("href")
        self.have_info = True

    @property
    def hash(self):
        regex = re.compile(r"\burn:btih:([A-F\d]+)\b", re.IGNORECASE)
        match = regex.search(self.magnet)
        if not match:
            print(self.name, self.magnet)
            return ""
        return match.group(1).strip()
