#!/bin/python3
import asyncio
import json
import sys
from types import SimpleNamespace

import httpx
from fuzzywuzzy import process as fz


async def update(game_id, banner_id, client):
    url = f"https://bkftwbhopivmrgzcagus.supabase.co/rest/v1/game?id=eq.{game_id}"
    headers = {
        "apikey": sys.argv[1],
        "Content-Type": "application/json",
        "Prefer": "return=representation",
    }
    data = json.dumps({"banner_id": banner_id})
    print(await client.patch(url, headers=headers, data=data))


async def main():
    games_url = (
        "https://bkftwbhopivmrgzcagus.supabase.co/rest/v1/game?select=id,name,banner_id"
    )
    banners_url = (
        "https://bkftwbhopivmrgzcagus.supabase.co/rest/v1/banner?select=id,rel_path"
    )
    headers = {"apikey": sys.argv[1]}
    games = [
        SimpleNamespace(**game) for game in httpx.get(games_url, headers=headers).json()
    ]
    banners = [
        SimpleNamespace(**banner)
        for banner in httpx.get(banners_url, headers=headers).json()
    ]
    tasks = []
    async with httpx.AsyncClient() as client:
        for game in games:
            if game.banner_id is not None:
                continue
            banner_name = fz.extractOne(
                game.name, (banner.rel_path for banner in banners)
            )
            if banner_name and banner_name[1] >= 90:
                banner_id = [
                    banner for banner in banners if banner.rel_path == banner_name[0]
                ][0].id
                tasks.append(asyncio.create_task(update(game.id, banner_id, client)))
        await asyncio.gather(*tasks)


asyncio.run(main())
