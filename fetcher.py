import re
from typing import Optional

import trio
from scalpel import Configuration
from scalpel.any_io import StaticResponse, StaticSpider, read_mp

BASE_URL = "https://1337x.to"
PAGE_URL = f"{BASE_URL}/johncena141-torrents/{{}}/"
CONFIG = Configuration(backup_filename="games.mp")


def cleanhtml(raw_html):
    cleanr = re.compile("<.*?>")
    cleantext = re.sub(cleanr, "", raw_html)
    return cleantext


def strip(x: Optional[str]) -> Optional[str]:
    if x is not None:
        return x.strip()
    return x


class ItemParser:
    def parse_info(self, node):
        x = node.xpath("..").get()
        if not x:
            return
        infos = cleanhtml(node.xpath("..").get().replace("<br>", "\n")).strip()
        infos = infos.split("\r\n\n")
        for info in infos:
            if info.lower().startswith("info"):
                regex = re.compile(r"Genre:(.*)")
                match = regex.search(info)
                if match:
                    self.genres = [genre.strip() for genre in match.group(1).split(",")]
                regex = re.compile(r"Language:(.*)")
                match = regex.search(info)
                if match:
                    self.languages = [
                        lang.strip() for lang in match.group(1).split(",")
                    ]
            if info.lower().startswith("system requirements"):
                self.reqs = {}
                regex = re.compile(r"(.*):(.*)")
                for key, value in regex.findall(info):
                    self.reqs[key.strip()] = value.strip()
            if info.lower().startswith("description"):
                self.description = info.splitlines()[1]

    def parse_tags(self, tags):
        self.tags = []
        regex = re.compile(r"\[(.*?)\]")
        for tag in regex.findall(tags):
            self.tags.append(tag.strip())
        self.version = regex.sub("", tags).strip() or "unknown"

    async def __call__(self, spider: StaticSpider, response: StaticResponse) -> None:
        self.name = self.description = self.tags = self.size = self.hash = ""
        self.genres = self.languages = list()
        self.name = strip(
            response.xpath('//*[@id="description"]/p/span[1]/strong/text()').get()
        )
        self.parse_tags(
            strip(response.xpath('//*[@id="description"]/p/span[2]/text()').get())
        )
        self.size = strip(
            response.xpath(
                "/html/body/main/div/div/div/div[2]/div[1]/ul[2]/li[4]/span/text()"
            ).get()
        )
        self.hash = strip(
            response.xpath('//*[@class="infohash-box"]/p/span/text()').get()
        )
        self.parse_info(
            response.xpath('//*[@id="description"]//strong[contains(text(),"Info")]')
        )
        data = {
            "name": self.name,
            "size": self.size,
            "description": self.description,
            "genres": self.genres,
            "languages": self.languages,
            "hash": self.hash,
            "tags": self.tags,
            "version": self.version,
        }
        await spider.save_item(data)


class TableParser:
    async def __call__(self, spider: StaticSpider, response: StaticResponse) -> None:
        urls = []
        for url in response.xpath(
            "/html/body/main/div/div/div[3]/div[1]/table/tbody/tr/td[1]/a[2]/@href"
        ):
            urls.append(BASE_URL + url.get())

        # parse torrent
        spider = StaticSpider(urls=urls, parse=ItemParser(), config=CONFIG)
        await spider.run()

        # next_link = response.xpath(
        #     '//div[@class="pagination"]/ul/li[last()-1]/a/@href'
        # ).get()
        # if next_link is not None:
        #     await response.follow(BASE_URL + next_link)


async def get_page(page_no: int = 1):
    spider = StaticSpider(urls=[PAGE_URL.format(page_no)], parse=TableParser())
    await spider.run()
    games = []
    async for data in read_mp("games.mp", decoder=CONFIG.msgpack_decoder):
        games.append(data)

    return games


if __name__ == "__main__":
    trio.run(get_page)
